module.exports = {
  pathPrefix: `/gatsby`,
  siteMetadata: {
    title: `美好生活`,
  },
  plugins: [`gatsby-plugin-react-helmet`],
}
